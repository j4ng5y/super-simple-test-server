package main

import (
	"context"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
)

type hndlr struct {
	Content []byte
}

func (h *hndlr) setContent(c []byte) {
	h.Content = c
}

func (h *hndlr) setContentFromFile(filePath string) error {
	s, err := os.Stat(filePath)
	if err != nil {
		return err
	}
	if s.IsDir() {
		return fmt.Errorf("'%s' is a directory, not a file", filePath)
	}
	f, err := os.Open(filePath)
	if err != nil {
		return err
	}
	b, err := ioutil.ReadAll(f)
	if err != nil {
		return err
	}
	h.Content = b
	return nil
}

func (h hndlr) handler(w http.ResponseWriter, r *http.Request) {
	_, err := w.Write(h.Content)
	if err != nil {
		log.Printf("an error occurred, err: %+v", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func main() {
	var runChan = make(chan os.Signal, 1)
	var addrFlag, contentFileFlag string
	var portFlag, idleTimeoutFlag, rxTimeoutFlag, txTimeoutFlag int

	flag.StringVar(&contentFileFlag, "content-file", "", "A file to present with the web server")
	flag.StringVar(&addrFlag, "addr", "0.0.0.0", "the address to bind to")
	flag.IntVar(&portFlag, "port", 80, "The port to use")
	flag.IntVar(&idleTimeoutFlag, "idle-timeout", 60, "The port to use")
	flag.IntVar(&rxTimeoutFlag, "read-timeout", 15, "The port to use")
	flag.IntVar(&txTimeoutFlag, "write-timeout", 15, "The port to use")
	flag.Parse()

	ctx, cancel := context.WithTimeout(context.Background(), time.Duration(idleTimeoutFlag)*time.Second)
	defer cancel()

	router := mux.NewRouter()

	switch {
	case contentFileFlag != "":
		h := &hndlr{}
		if err := h.setContentFromFile(contentFileFlag); err != nil {
			log.Fatal(err)
		}
		router.Handle("/", http.HandlerFunc(h.handler))
	default:
		h := &hndlr{
			Content: []byte("successful"),
		}
		router.Handle("/", http.HandlerFunc(h.handler))
	}

	server := &http.Server{
		Addr:         fmt.Sprintf("%s:%d", addrFlag, portFlag),
		IdleTimeout:  time.Duration(idleTimeoutFlag) * time.Second,
		WriteTimeout: time.Duration(txTimeoutFlag) * time.Second,
		ReadTimeout:  time.Duration(rxTimeoutFlag) * time.Second,
		Handler:      router,
	}

	signal.Notify(runChan, os.Interrupt, syscall.SIGTERM, syscall.SIGTSTP)

	log.Printf("Server is running on %s", server.Addr)
	go func() {
		if err := server.ListenAndServe(); err != nil {
			if err == http.ErrServerClosed {
				// Normal Operation
			} else {
				log.Fatal(err)
			}
		}
	}()

	<-runChan

	log.Print("Server shutting down due to interrupt")
	if err := server.Shutdown(ctx); err != nil {
		log.Fatal(err)
	}
	log.Print("Done")
}
