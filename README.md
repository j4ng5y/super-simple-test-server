# Super Simple Test Server

This is a super tiny http server that does nothing fancy.

## Usage

```
Usage of super simple test server:
  -addr string
        the address to bind to (default "0.0.0.0")
  -content-file string
        A file to present with the web server
  -idle-timeout int
        The port to use (default 60)
  -port int
        The port to use (default 80)
  -read-timeout int
        The port to use (default 15)
  -write-timeout int
        The port to use (default 15)
```